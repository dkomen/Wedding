﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Packages.aspx.cs" Inherits="Packages" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>
        .close 
        {            
            text-align:center;
            position:relative;top:3px;
            display:inline-block;
		    background: #505051;
		    color: #FFFFFF;   
            width:180px;
            height:20px;
		    -webkit-border-radius: 18px;
		    -moz-border-radius: 18px;
		    border-radius: 18px;
		    -moz-box-shadow: 1px 1px 3px #000;
		    -webkit-box-shadow: 1px 1px 3px #000;
		    box-shadow: 1px 1px 3px #000;
            line-height:20px;
            font-size:10pt; 
            text-decoration:none;   
            font-family: Arial, Helvetica, sans-serif;        
	    }
	    .close:hover { background: #808081; }
    </style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <center>
        <div style="text-align:left; margin: 30px; padding:20px; width:750px; background-color:rgba(0,0,0,0.5); border-radius:12px;font-family: Arial, Helvetica, sans-serif; font-size:10pt; border-style:solid; border-width:3px; border-color:#777; color: wheat" >
            <center><a href="default.aspx" title="Close" class="close" style="position:relative;top:-30px">click to return to portfolio</a></center>
            <span style="color:green; font-weight:bold">RUBY - R3000-00</span><br />
            100+ edited images (various styles)<br />
            -4.5 hours with photographer<br />
            -DVD with slide-show of high resolution images (all are of print quality)<br />
            -Website for 6 months with all images<br />
                <br />
            <span style="color:red; font-weight:bold">EMERALD - R5000-00</span><br />
            175+ edited images (various styles)<br />
            -112 Prints (100x Jumbo, 10x 6x9, 2x A4)<br />
            -7 hours with photographer<br />
            -DVD with slide-show of high resolution images (all are of print quality)<br />
            -Website for 6 months with all images<br />           
            <br />
            <br />
            <br />
            <b>General notes</b> <br />
            All wedding packages contain a mix of artistically modified, black-and-white as well as only slightly developed images <br />
            <br /><br />
            If it turns out on the day that you wish to keep the photographer for a period that is longer than your package specified number of hours then the additional rate will be calculated at R125-00 per additional half-hour. <br />
            <br /><br />
            <b>Wedding story books</b>
            Wedding story book options vary and may include leather covers too. <br />
            <br /><br />
            <b>Price fixture and deposits</b>
            Prices may change without any notice and only once a deposit of 35% of the package price is made will the quoted prices be fixed. The balance is payable on or before the day you receive your photographic package. <br />
            If you wish to have your photographic package couriered to you then the balance is payable before said items are sent. <br />
            No cheques or credit cards accepted. <br />
            <br /><br />
            <b>Travelling costs</b> 
            All prices exclude travelling outside the immediate Pretoria-east and Centurion areas and are calculated at an additional R2-00 per kilometer tavelled (based on AA rates). A wedding in the Fourways area will therefore cost perhaps an additional R180-00 and R220-00 for the MuldersDrif area. For air travel you would need to arrange for tickets. You would also need to arrange for overnight etc. accomodation if so needed. 
            <br /><br />
            <b>Framing</b>
            Prices range from R100-00 for A5 or 6x9 table standing frames to about R850-00 for high quality frames for A4 size photos with a mounting board of about 5cm around the photo. 
            <br /><br /><br />
            Please note that I reserve the right to use any tastefull photos for portfolio reasons unless you request me, on a mutually signed document, not to.
            </br><center><a href="default.aspx" title="Close" class="close" style="position:relative;top:30px">click to return to portfolio</a></center>
        </div>
    </center>
</asp:Content>


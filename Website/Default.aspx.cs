﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string[] thumbs = System.IO.Directory.GetFiles(Server.MapPath("/wedding/Slides/Thumbs"));
            
            string imageArrays = "<script type = \"text/javascript\">";

            int i=0;
            int leftPos = 0;
            int maxHeight = 0;
            for (int thumbIndex = 0; thumbIndex < thumbs.Length;thumbIndex++)
            {
                string thumb = thumbs[thumbIndex];
                System.Drawing.Image img = System.Drawing.Image.FromFile(thumb.Replace("Thumbs", "Large"));
                uxGallery.InnerHtml += "<span class='slide'>" +
                                            "<a href='#openModal' onclick='SetCurrent(" + i + ");' style='border-color:red;'>" +
                                                "<img class='slideImage' src='/wedding/Slides/Thumbs/" + System.IO.Path.GetFileName(thumb) + "' />" +
                                            "</a>" +
                                       "</span>";
                imageArrays += "largeArray[" + i + "]='/wedding/Slides/Large/" + System.IO.Path.GetFileName(thumb) + "';";

                i++;
                System.Drawing.Image thumbImg = System.Drawing.Image.FromFile(thumb);
                leftPos += thumbImg.Width;
                if (maxHeight < thumbImg.Height)
                {
                    maxHeight = thumbImg.Height;
                }                
            }
            imageArrays += "uxGallerySurround.style.top=(((window.innerHeight/2) - (" + maxHeight + "/2))-(window.innerHeight*0.25)) + 'px';";
            imageArrays += "gs.style.height = '" + maxHeight + "px';</script>";
            ClientScript.RegisterStartupScript(this.GetType(),"", imageArrays);
        }
    }
}
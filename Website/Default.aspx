﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style>       
        .gallerySurround
        {
            margin: auto 0 0; padding:0px;
            /*background-image: -webkit-gradient(linear, left bottom, left top, color-stop(0.33, rgb(3, 12, 86)), color-stop(0.67, rgb(0,0,255)));
            background-image: -moz-linear-gradient(center bottom, rgb(3, 12, 86) 33%, rgb(0,0,255) 67% );*/
            border-style:solid;border-top-width:8px;border-bottom-width:6px;border-bottom-color:#444;border-top-color:#111;
            background-color:#222;
            width:100%; 
            position:relative;  
            text-align:center;  
            top:150px;    
        }

        .gallery
        {                                                  
            white-space:nowrap;                           
            overflow-x: scroll;   
            overflow-y: hidden;
        }

        .slide
        {                                                                                                   
            margin:0px;padding:0px;
        }        
      
        .slideImage 
        {
            border-width:0px;
            border-style:hidden;
        }

        .modalDialog 
        {
            text-align:center;
            background-color:black;
		    position: fixed;
		    font-family: Arial, Helvetica, sans-serif;
		    top: 0;
		    right: 0;
		    bottom: 0;
		    left: 0;
		    background: rgba(0,0,0,0.8);
		    z-index: -1;
		    opacity:0;
            /*background: none;*/          
             -webkit-transition: all 600ms ease-in-out;
		    -moz-transition: all 600ms ease-in-out;
		    transition: all 600ms ease-in-out;
		    pointer-events: none;
	    }

	    .modalDialog:target 
        {                      
		    opacity: 90;
		    pointer-events: auto;
            z-index: 9999;
	    }

	    .modalDialogSpan 
        {      
            position:relative;
            display:inline-block;
            margin: 3% auto 0 0;
		    padding: 12px;           
		    border-radius: 8px;
		    background: #fff;
		    background: -moz-linear-gradient(#fff, #999);
		    background: -webkit-linear-gradient(#fff, #999);
		    background: -o-linear-gradient(#fff, #999);            
	    }

	    

        
        .navigationNext
        {
            border-bottom-style:solid;border-width:2px;border-color:white;
            -webkit-border-radius: 18px;
		    -moz-border-radius: 18px;
		    border-radius: 18px;
            line-height:35px;
            display:inline-block;
		    background: #525252;
		    color: #FFFFFF;   
		    width: 90px;
            height:35px;
            font-size:12pt;		    
		    -moz-box-shadow: 1px 1px 3px #000;
		    -webkit-box-shadow: 1px 1px 3px #000;
		    box-shadow: 1px 1px 3px #000;            
            text-decoration:none;        
            position:absolute;top:30px;left:-14px;                                                      
        }
        .navigationPrevious
        {
            border-bottom-style:solid;border-width:2px;border-color:white;
            -webkit-border-radius: 18px;
		    -moz-border-radius: 18px;
		    border-radius: 18px;
            line-height:35px;
            display:inline-block;
		    background: #525252;
		    color: #FFFFFF;   
		    width: 90px;
            height:35px;
            font-size:12pt;		    
		    -moz-box-shadow: 1px 1px 3px #000;
		    -webkit-box-shadow: 1px 1px 3px #000;
		    box-shadow: 1px 1px 3px #000;   
            position:absolute;top:30px;right:-14px;             
        }        
        .close 
        {            
            text-align:center;
            position:relative;top:3px;
            display:inline-block;
		    background: #505051;
		    color: #FFFFFF;   
            width:180px;
            height:20px;
		    -webkit-border-radius: 18px;
		    -moz-border-radius: 18px;
		    border-radius: 18px;
		    -moz-box-shadow: 1px 1px 3px #000;
		    -webkit-box-shadow: 1px 1px 3px #000;
		    box-shadow: 1px 1px 3px #000;
            line-height:20px;
            font-size:10pt; 
            text-decoration:none;    
            font-family: Arial, Helvetica, sans-serif;       
	    }
	    .close:hover { background: #808081; }
        
        a, img 
        {
            border:none;
        }       
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <audio autoplay loop>
        <source src="/wedding/Music/Richard Claydermann - Ballade Pour Adeline.mp3" />
    </audio>

    <a href='packages.aspx' class="close" style="border-radius:2px;font-size:12pt">Packages and pricing</a>            
    <a href='/windingart' class="close" style="border-radius:2px;font-size:12pt">Fine-art galleries</a>        

    <div class="gallerySurround" id="uxGallerySurround">
        <div id="uxGallery" runat="server" class="gallery">
        </div>         
        <span class="close" style="width:285px;align-content:center;top:12px;">
            click on image to view the larger version
        </span>
    </div>    
    <div id="openModal" class="modalDialog">
        <a href="#none" style="text-decoration:none;color: #FFFFFF;" onclick="SetNextImage(-1);return false;" class="navigationNext">prev</a>                                      
        <a href="#none" style="text-decoration:none;color: #FFFFFF;" onclick="SetNextImage(1);return false;" class="navigationPrevious">next</a>
	    <span class="modalDialogSpan">		                            
            <a href="#close"><img src="" id="uxImage" /></a>     
            <br />  
                <a href="#close" title="Close" class="close">
                    click on image to close
                </a>                           
	    </span>        
    </div>

    <!--[if IE]>
        <br />
        <center>
            <a href="#close" title="Close" class="close" style="width:340px;position:relative;top:100px;left:3px;">
                Please use a browser other than Internet Explorer
            </a>
        </center>
    <![endif]-->
    <script type = "text/javascript">
        var thumbsArray = new Array();
        var largeArray = new Array();
        var currentImage = 0;
        var gs = document.getElementById('uxGallery');
        var uxGallerySurround = document.getElementById('uxGallerySurround');
        
        function SetCurrent(imageId) {
            currentImage = imageId;
            image = document.getElementById('uxImage');            
            image.src = largeArray[currentImage];
            if (window.screen.availWidth < image.width - 35) {
                image.width = window.screen.availWidth - 45;
            }
            if (window.screen.availHeight < image.height - 35) {
                image.height = window.screen.height - 95;
            }
            
            image2 = new Image();
            if (currentImage <= largeArray.length-1) {
                
                image2.src = largeArray[currentImage + 1];
            }

            if (currentImage >= 1)
            {
                image3 = new Image();
                image3.src = largeArray[currentImage - 1];
            }            
        }
        function SetNextImage(nextImageOffset) {
            if ((currentImage + nextImageOffset) < 0)
            {
                SetCurrent(largeArray.length-1);
            }
            else if ((currentImage + nextImageOffset) > largeArray.length-1) {
                SetCurrent(0);
            }
            else {
                SetCurrent(currentImage + nextImageOffset);
            }
        }

        //var audio = document.createElement("audio");

        //if (audio != null && audio.canPlayType && audio.canPlayType("audio/mpeg")) {

        //    audio.src = "music/x.mp3";

        //    audio.play();
        //    if (audio.canPlayType("mp3")) {
        //        alert('yes');
        //    }
        //}
    </script>
</asp:Content>

